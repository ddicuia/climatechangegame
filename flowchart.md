```flow
st=>start: Start 
op0=>operation: For each character
op3=>operation: Augment level of alert
op1=>operation: Start countdown
op2=>operation: On countdown end
cond=>condition: Is the question being answered?
op2b=>operation: On question answered
cond2=>condition: Has alert reached the max?
op4=>operation: Reset level of alert
op4b=>operation: Update system
op5=>operation: Go to next question
cond3=>condition: Are questions over?
e=>end: Game Over

st->op0->op1->op3->op2->cond
cond(no)->cond2
cond(yes)->op2b->op4->op4b->op5->cond3
cond2(no)->op1
cond2(yes)->e
cond3(yes)->e
```

