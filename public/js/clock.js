var now = moment();
var hour = now.hours();
var minute = now.minutes();
var second = now.seconds();

var canvas = document.getElementById("clock");
var ctx = canvas.getContext("2d");
var radius = canvas.height*.45;
ctx.translate(radius,radius);

var clockInterval = setInterval(tick, 1000);

function drawClock() {
	ctx.save();
	ctx.translate(10,10);
	drawFace(ctx, radius);
	// drawNumbers(ctx, radius);
	drawTime(ctx,radius);
	drawNose(ctx,radius);
	ctx.restore();
}

function drawFace(ctx, radius) {
	ctx.beginPath();
	ctx.arc(0, 0, radius, 0, 2*Math.PI);
	ctx.fillStyle = "#F8F8FF";
	ctx.fill();

	ctx.beginPath();
	ctx.arc(0, 0, radius, 0, 2*Math.PI);
	ctx.lineWidth = 5;
	ctx.strokeStyle = "#333";
	ctx.stroke();

}


function tick() {
	now = moment();
	drawClock();
}

// Draw the Hands depends on current time
function drawTime() {
	
	hour = now.hours();
	minute = now.minutes();
	second = now.seconds();


	// Draw the Hour Hand
	hour=hour%12;
	hour=(hour*Math.PI/6)+(minute*Math.PI/(6*60))+(second*Math.PI/(360*60));
	drawHand(ctx, hour, radius*0.6, 4, "black");
	// Draw the Minute Hand
	minute=(minute*Math.PI/(30))+(second*Math.PI/(30*60));
	drawHand(ctx, minute, radius*0.75, 2, "black");
	// Draw the Second Hand
	second=(second*Math.PI/30);
	drawHand(ctx, second, radius*0.9, 1, "#DC143C");
}

// Define how to draw the Hands
function drawHand(ctx, pos, length, width, color){
	ctx.beginPath();
	ctx.lineWidth = width;
	ctx.lineCap = "round";
	ctx.moveTo(0,0);
	ctx.rotate(pos);
	ctx.lineTo(0, -length);
	ctx.strokeStyle = color;
	ctx.stroke();
	ctx.rotate(-pos);
}

function drawNose(ctx, radius) {
	ctx.beginPath();
	ctx.arc(0, 0, radius*.08, 0, 2*Math.PI);
	ctx.fillStyle = "#DC143C";
	ctx.fill();
}

function advanceTime(callback) {
	clearInterval(clockInterval);
	var obj = {minute:minute};
	now = moment().hours(hour);
	var addHours = Math.round(Math.random() * 30) + 10;
	TweenMax.to(obj, 5, {hour: hour+ addHours, minute: minute+ addHours*60, onUpdate: function() {
		now = moment().minutes(obj.minute);
		drawClock();

	}, onComplete: function() {
		clockInterval = setInterval(tick, 1000);
		callback();
	}.bind(this)})
}