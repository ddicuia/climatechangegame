var DEBUG = window.location.hash === "#debug";
if(DEBUG) {
	$("#state").show();
	$("#test").show();
}
var state = {
	currentCharacter: -1,
	properties:{},
	currentPresident: "danald",
	characters: [
	{
		name:'individual',
		realName:'Joe',
		currentActionIndex: 0,
		lastChecked: null,
		alert: 0,
		status:20,
		isAsking: false,
		id: "1xIxjPpaaG3jeNPcJVfZThNy-Y-I14xP9xH1G_Qp4TYo"
	},
	{
		realName:'Linda',
		name:'business',
		currentActionIndex: 0,
		isAsking: false,
		lastChecked: null,
		alert: 0,
		status:20,
		id: "1Or6rtLUgLG1R5uBeySJNc54qDKCB-xC-KF1wZF85CUo"
	},
	{
		realName:'Malena',
		name:'policymakerlocal',
		lastChecked: null,
		alert: 0,
		status:20,
		isAsking: false,
		currentActionIndex: 0,
		id:"1k3k5XcOutiKRREiy9SwgI0pUqFfmTfhcWsdfQ5n2eOw"
	},
	{
		realName:'The president',
		name:'danald',
		lastChecked: null,
		alert: 0,
		status:20,
		isAsking: false,
		currentActionIndex: 0,
		id: "1yPExe6lGFY4u_6J6JP8qlv1_cX0JlV4Y5YRqp_5KIb4"
	}
	]
};

var labels = ["Food shortage", "Heat", "Water", "Economy",	"Pollution", "Self","link", "seconds"];
var $state = $("#state");
var $buttonA = $("#buttonA");
var $buttonB = $("#buttonB");
var $charsButtons;
var stop = false;
var votingTimeout;
var carachterCompleted = 0;
var audio = new Audio('img/04AllofUs.mp3');
audio.addEventListener('ended', function() {
    this.currentTime = 0;
    this.play();
}, false);

var imagesListPreload = ['img/city-base.png',
'img/degraded-city.png',
'img/future-city.png',
'img/future-city-polluted.png',
'img/city-base-underwater.png',
'img/degraded-city-underwater.png',
'img/future-city-underwater.png',
'img/future-city-polluted-underwater.png',
'img/city-base-just-water.png',
'img/degraded-city-just-water.png',
'img/future-city-just-water.png',
'img/future-city-polluted-just-water.png',
'img/smoke.png',
'img/city-base-quter-towards-pollution.png',
'img/city-base-half-towards-pollution.png',
'img/degraded-city-quater-towards-base.png',
'img/degraded-city-less-than-quater-towards-base.png',
'img/future-city-1.png',
'img/future-city-2.png',
'img/future-city-3.png',
'img/future-city-4.png',
'img/future-city-5.png',
'img/future-city-6.png',
'img/future-city-7.png',
'img/future-city-8.png',
'img/future-city-pollution-1.png',
'img/future-city-pollution-2.png',
'img/future-city-pollution-3.png'
];

function getJson(onComplete) {


	$.when(
		$.getJSON("https://spreadsheets.google.com/feeds/list/" + state.characters[0].id + "/od6/public/values?alt=json"),
		$.getJSON("https://spreadsheets.google.com/feeds/list/" + state.characters[1].id + "/od6/public/values?alt=json"),
		$.getJSON("https://spreadsheets.google.com/feeds/list/" + state.characters[2].id + "/od6/public/values?alt=json"),
		$.getJSON("https://spreadsheets.google.com/feeds/list/" + state.characters[3].id + "/od6/public/values?alt=json")	
		)
	.done(function(result1, result2, result3, result4) {
		state.characters[0].data = parseJsonreturnList(result1[0]);
		state.characters[1].data = parseJsonreturnList(result2[0]);
		state.characters[2].data = parseJsonreturnList(result3[0]);
		state.characters[3].data = parseJsonreturnList(result4[0]);
		onComplete();
	});

	
}

function parseJsonreturnList(data){
	var rows = data.feed.entry,
	localMyData = [];

	var p = "gsx$";

	for (var k = 0; k < rows.length; k+=2) {

		var d = rows[k];
		var dd = rows[k+1];

		if(d[p+"action"] == undefined) continue;

		var obj = {
			title: d[p+"action"] ?  d[p+"action"].$t: "  ",
			seconds:  parseInt(d[p+"seconds"].$t),
			answers: [{
				title: d[p+"answer"].$t,
				values: {}
			},
			{
				title: dd[p+"answer"].$t,
				values: {}
			}]
		}

		for (var i = 0; i < labels.length; i++) {
			var l  = labels[i].replace(" ", "").toLowerCase();
			obj.answers[0].values[l] =  parseInt(d[p +l].$t);
			obj.answers[1].values[l] =  parseInt(dd[p + l].$t);
		}

		localMyData.push(obj)
	}

	return localMyData;
}

function init() {
	
	// add the characters with their images 
	for(var i =0; i< state.characters.length; i++) {
		var d = state.characters[i];
		var $p = $("<div class='character' id='"+ d.id +"'/>");
		$p.append($("<div class='question'>?</div>"));
		$p.append($("<img src='img/"+ d.name +".png'/>"));
		var $status = $("<div class='statusbar'/>");
		$status.append($('<span class="statusbarfill"/ >'));
		$p.append($status);
		$("#characterUiContainer").append($p);
		state.characters[i].el = $p;
	}
	$charsButtons = $("#characterUiContainer .character");
	$charsButtons.each(function(i,el) {
		$(el).on("click", onQuestionClicked.bind(this, i));
	});

	// answerbuttons
	$buttonA.on("click", function(){ onAnswerClicked(0);});
	$buttonB.on("click", function(){ onAnswerClicked(1);});
	$("#instructions").show();
	
	// modal buttons
	$("#instructions .okbtn").on("click", function(){
		$("#instructions").hide();
		startGame();
	}.bind(this));

	$("#gameover .okbtn").on("click", function(){
		$("#gameover").hide();
		restartGame();
	}.bind(this));

	render();

}

function startGame() {

	// start timers for each character		
	for(var i =0; i< state.characters.length; i++) {
		var c = state.characters[i];
		c.lastChecked = new Date();
		var timerSeconds = DEBUG ? 2 : c.data[0].seconds;
		state.characters[i].timer = new Timer().on('end',onCharacterQuestionTimeOut.bind(this,i)).start(timerSeconds);
		renderQuestionAlert(i);
		renderCharacterStatus(i);
	}

	state.properties.water = 10;
	// start voting countdown
	votingTimeout = setTimeout(votingTime, 60000);

}

function restartGame() {
	for (var i = 0; i < state.characters.length; i++) {
		state.characters[i].currentActionIndex = 0;
		state.characters[i].lastChecked=null;
		state.characters[i].alert= 0;
		state.characters[i].status=20;
		state.characters[i].isAsking= false;
	}
	startGame();
}

function onCharacterQuestionTimeOut(characterIndex) {
	update({type:"needsAnswer", character:characterIndex});
}

function onQuestionClicked(characterIndex) {
	if(state.characters[characterIndex].alert>0) update({type:"questionClicked", character:characterIndex});
}

function onAnswerClicked(value) {
	showQuestion(false);
	hideButtons();
	update({type:"answerClicked", value:value});
}

function update(o) {

	if(stop) return;

	var type = o.type;
	if(!type) {
		alert("Missing update type"); 
		return
	}


	switch(type) {

		case "questionClicked":
		if(o.character.alert == 0) return; // this prevents clicking when no alert is shown
		var i = o.character;
		var c = state.characters[i];
		state.currentCharacter = i;
		pauseCharactersTimers();
		c.isAsking = true;
		render();
		break;

		case "needsAnswer":
		var i = o.character;
		var c = state.characters[i];
		if(!c.isAsking) {
			c.alert++;			
			// set the question alert
			renderQuestionAlert(i);
			c.timer.start();
		}
		break;

		case "answerClicked":
		var c = state.characters[state.currentCharacter];
		var currentActionIndex = c.currentActionIndex;
		c.isAsking = false;
		c.lastChecked = new Date();
		c.alert = 0;
		
		var answerIndex = o.value;
		c.currentActionIndex++;

		if(c.currentActionIndex < c.data.length) {
			
			var d = c.data[currentActionIndex];
			
			// update state properties
			for(var i in d.answers[answerIndex].values) {
				if(!state.properties[i]) state.properties[i] = 0;
				state.properties[i] += d.answers[answerIndex].values[i];
			}
			
			c.status += d.answers[answerIndex].values.self;
			renderCharacterStatus(state.currentCharacter);
			var timerSeconds = DEBUG ? 2 : d.seconds;
			c.timer = new Timer().on('end',onCharacterQuestionTimeOut.bind(this,state.currentCharacter)).start(timerSeconds);
		} else {
			carachterCompleted++;
			if(carachterCompleted == 4) {
				endGame();
			}
		}

		renderQuestionAlert(state.currentCharacter);
		resumeCharactersTimers();

		// resetting character
		state.currentCharacter = -1;
		render();
		renderCity();
		break;

		case "voted":
		// 50% chance the president will be the one you chose
		var chance  = DEBUG ? 1 : Math.random();
		var president = chance > 0.5 ? o.president : (o.president === 0 ? 1 : 0);
		state.currentPresident = president;
		break;

		default:
		alert(o.type + "Invalid Update");

	}

	console.log(o);

}

function votingTime() {
	clearTimers();
	$("#voting").show();
	showQuestion(false);
}

function voteFor(n) {
	$("#votingtime").hide();
	$("#calculatingvotes").show();
	update({type:"voted", president:n});
	setTimeout(function() {
		$("#calculatingvotes").hide();
		$("#voteresults h1").text((state.currentPresident === 0 ? "John Rain" : "Danald") + " won!");
		$("#voteresults").show();
		if(state.currentPresident === 0) $("#characterUiContainer img:eq(3)").attr("src", "img/asiangirl.png")
	}, 2000);
}

function votingDone() {
	$("#voting").hide();
	resumeCharactersTimers();
}

function renderCharacterStatus(i) {
	var c = state.characters[i];
	var w = $(".statusbar", c.el).width();
	$(".statusbarfill", c.el).width(c.status/40 *w);
}

function renderQuestionAlert(i) {
	var c = state.characters[i];
	var el = state.characters[i].el;
	var currentTime  = new Date();

	$(".question", el).removeClass("shake");
	
	el.toggleClass("active", c.alert>0);

	var opacity;

	var elapsedTime = currentTime - state.characters[i].lastChecked;
	if(c.alert === 0) {
		opacity = 0;
	} else if(c.alert == 1) {
		opacity = 0.2;
	} else if(c.alert == 2) {
		opacity = .5;
	} else if(c.alert == 3) {
		opacity = 1;
	} else if(c.alert == 4)  {
		opacity = 1;
		$(".question", el).addClass("shake");
	} else {
		gameOver(i);
	}

	$(".question", el).css({opacity:opacity});
}

function gameOver(characterIndex) {
	clearTimers();
	showQuestion(false);
	$("#gameover").show();
	$("#gameover #losingcharacter").text(state.characters[characterIndex].realName);
}
function endGame() {
	clearTimers();
	$("#uiContainer").hide();
	$("#debug").hide();
	$("#pollutionlevel").text(getAdjective(state.properties.pollution)); 
	$("#heatlevel").text(getAdjective(state.properties.heat));
	$("#economylevel").text(getAdjective(state.properties.economy));
	$("#foodlevel").text(getAdjective(state.properties.food));
	$("#waterlevel").text(getAdjective(state.properties.water));
		

	TweenMax.to($("#imageContainer"), 1, {top: "-100px", left:"0", onComplete:function() {
		$("#endgame").show();
	}});
}

function getAdjective(n) {
	if(n >= 0 && n <5) {
		return "very low";
	} else if(n >= 5 && n <10) {
		return "low";
	} else if(n >= 10 && n <20) {
		return "high";
	} else if(n >= 20 && n <22) {
		return "very high";
	} else {
		return "extremely high";
	}
	
}

function pauseCharactersTimers() {
	for (var i = 0; i < state.characters.length; i++) {
		state.characters[i].timer.pause();
	}
}

function resumeCharactersTimers() {
	for (var i = 0; i < state.characters.length; i++) {
		var c =state.characters[i];
		if(c.currentActionIndex < c.data.length) {
			var timerSeconds = DEBUG ? 2 : c.data[c.currentActionIndex].seconds;
			c.timer.start(timerSeconds);
		}
	}
}

function clearTimers() {
	for (var i = 0; i < state.characters.length; i++) {
		state.characters[i].timer.stop();
	}
	clearTimeout(votingTimeout);
}


function render() {

	// state
	var s = "<ul>";
	for(var i in state.properties) {
		if(i!="seconds" && i!="self" && i!="link") s+= "<li><b>"+i+":</b> "+ state.properties[i] + "</li>";
	}
	s += "<ul>";
	$state.html(s);


	if(state.currentCharacter != -1) {
		// character view
		var currentCharacter = state.characters[state.currentCharacter];
		var atLastAction = currentCharacter.currentActionIndex == currentCharacter.data.length;
		// currentCharacter.mana
		

		// buttons
		if(!atLastAction) {
			var currentAction = currentCharacter.data[currentCharacter.currentActionIndex];
			var w = window.innerWidth *0.25;
			var h = window.innerHeight*0.5;
			var characterImage =  "img/"+currentCharacter.name+".gif";
			if(currentCharacter.name == "danald" && state.currentPresident === 0) characterImage =  "img/asiangirl.gif";
			$("#textContainer img").attr("src",characterImage);

			$buttonA.text(unescape(currentAction.answers[0].title));
			$buttonB.text(unescape(currentAction.answers[1].title));

			if(DEBUG) {
				$buttonB.css({opacity: 1}, 200);
				$buttonA.css({opacity: 1}, 200);

			} else {
				$buttonB.delay( currentAction.title.length * 10 ).animate({opacity: 1}, 200);
				$buttonA.delay( currentAction.title.length * 10 ).animate({opacity: 1}, 200);				
			}


			$("#action").html(unescape(currentAction.title));
			showQuestion(true);

		} else {
			showQuestion(false)
		}
	}	
}

//Render variables
var maxPollution = 24,
maxEconomy = 24,
city = 'base';

function renderCity() {

	//city Selector

	if(state.properties.pollution > small_map_pollution(0.15) && state.properties.economy < small_map_economy(0.5) ) city = 'base';
	if(state.properties.pollution > small_map_pollution(0.30) && state.properties.economy < small_map_economy(0.5) ) city = 'base-degraded-quater';
	if(state.properties.pollution > small_map_pollution(0.45) && state.properties.economy < small_map_economy(0.5) ) city = 'base-degraded-half';
	if(state.properties.pollution > small_map_pollution(0.55) && state.properties.economy < small_map_economy(0.5) ) city = 'degraded-base-quater';
	if(state.properties.pollution > small_map_pollution(0.70) && state.properties.economy < small_map_economy(0.5) ) city = 'degraded-base-less-quater';	
	if(state.properties.pollution > small_map_pollution(0.95) && state.properties.economy < small_map_economy(0.5) ) city = 'degraded';

	if(state.properties.pollution < small_map_pollution(0.30) && state.properties.economy > small_map_economy(0.1)   ) city = 'future';
	if(state.properties.pollution < small_map_pollution(0.3) && state.properties.economy > small_map_economy(0.2)   ) city = 'future-1';
	if(state.properties.pollution < small_map_pollution(0.3) && state.properties.economy > small_map_economy(0.3)   ) city = 'future-2';
	if(state.properties.pollution < small_map_pollution(0.3) && state.properties.economy > small_map_economy(0.4)   ) city = 'future-3';
	if(state.properties.pollution < small_map_pollution(0.3) && state.properties.economy > small_map_economy(0.5)   ) city = 'future-4';
	if(state.properties.pollution < small_map_pollution(0.3) && state.properties.economy > small_map_economy(0.6)   ) city = 'future-5';
	if(state.properties.pollution < small_map_pollution(0.3) && state.properties.economy > small_map_economy(0.7)   ) city = 'future-6';
	if(state.properties.pollution < small_map_pollution(0.3) && state.properties.economy > small_map_economy(0.8)   ) city = 'future-7';
	if(state.properties.pollution < small_map_pollution(0.3) && state.properties.economy > small_map_economy(0.9)   ) city = 'future-8';

	if(state.properties.pollution > small_map_pollution(0.3) && state.properties.economy > small_map_economy(0.6)   ) city = 'future-polluted-1';
	if(state.properties.pollution > small_map_pollution(0.4) && state.properties.economy > small_map_economy(0.7)   ) city = 'future-polluted-2';
	if(state.properties.pollution > small_map_pollution(0.5) && state.properties.economy > small_map_economy(0.8)   ) city = 'future-polluted-3';
	if(state.properties.pollution > small_map_pollution(0.6) && state.properties.economy > small_map_economy(0.9)   ) city = 'future-polluted';


	//city Image 
	if(city==='base') imageTransition("img/city-base.png");
	if(city==='base-degraded-quater') imageTransition("img/city-base-quter-towards-pollution.png");
	if(city==='base-degraded-half')imageTransition("img/city-base-half-towards-pollution.png");
	if(city==='degraded-base-quater') imageTransition('img/degraded-city-less-than-quater-towards-base.png');
	if(city==='degraded-base-less-quater') imageTransition("img/degraded-city-quater-towards-base.png");
	if(city==='degraded')imageTransition("img/degraded-city.png");

	if(city=='future') imageTransition("img/future-city-8.png");
	if(city=='future-1') imageTransition("img/future-city-7.png");
	if(city=='future-2') imageTransition("img/future-city-6.png");
	if(city=='future-3') imageTransition("img/future-city-5.png");
	if(city=='future-4') imageTransition("img/future-city-4.png");
	if(city=='future-5') imageTransition("img/future-city-3.png");
	if(city=='future-6') imageTransition("img/future-city-2.png");
	if(city=='future-7') imageTransition("img/future-city-1.png");
	if(city=='future-8') imageTransition("img/future-city.png");

	if(city=='future-polluted-1') imageTransition("img/future-city-pollution-1.png");
	if(city=='future-polluted-2') imageTransition("img/future-city-pollution-2.png");
	if(city=='future-polluted-3') imageTransition("img/future-city-pollution-3.png");
	if(city=='future-polluted') imageTransition("img/future-city-polluted.png");

	//increase lower saturation TODO color for heatwave
	if(city.indexOf('future-polluted')>-1) citySaturation(1,0);
	else citySaturation(map_range(state.properties.pollution, 0, maxPollution, 2, 0),map_range(state.properties.pollution, 0, maxPollution, -150, 100));

	//increase decrease smoke 
	if(city.indexOf('future-polluted')>-1) imageSmokeTransition();
	else imageSmokeTransition('img/smoke.png',map_range(state.properties.pollution, 0, maxEconomy, -1.5, 1));

	//increase sun size 
	scaleSun(map_range(state.properties.pollution, 0, maxPollution, 80, 200));

	//increase decrease water water
	/*if(state.properties.pollution > small_map_pollution(0.85) ){

		if(city=='base') imageWaterTransition("img/city-base-just-water.png");
		if(city=='future') imageWaterTransition("img/future-city-just-water.png");
		if(city=='future-polluted')imageWaterTransition("img/future-city-polluted-just-water.png");
		if(city=='degraded')imageWaterTransition("img/degraded-city-just-water.png");
		
	} 		
	if(state.properties.pollution < small_map_pollution(0.85) ) imageWaterTransition();*/

}

function simulatechanges(name){
	stop = true;
	resetState();
	render();
	renderCity();
	cityChange(name,0);
}

function cityChange(name,x){

	x = x+1;
	var max = 0;

	switch(name){
		case 'pollution': max =  maxPollution;
		case 'economy': max =  maxEconomy;
		case 'economy-pollution': max = maxPollution;
	}

	if(x>max) { return;}	
	if(name != 'economy-pollution')state.properties[name] ++;
	if(name == 'economy-pollution'){
		state.properties['economy'] ++;
		state.properties['pollution'] ++;
	}

	renderCity();
	render();
	setTimeout(function() {cityChange(name,x)}, 1000);

}


function resetState(){
	
	//reset

	state.properties.pollution = 0;
	state.properties.water = 0;
	state.properties.heat = 0;
	state.properties.economy = 0;
	state.properties.foodshortage = 0;

}

function showQuestion(b) {
	$("#textContainer").toggle(b);
}

function hideButtons(){
	$("#textContainer button").css('opacity','0');
}

function imagePreloader(i){

	if(i==imagesListPreload.length) return; 
	imageObj = new Image();

	imageObj.src=imagesListPreload[i];

	imageObj.onload = imagePreloader(i+1);
} 

function imageTransition(name){
	var jImageDisappear = jQuery('.activeImage') ,
	jImageAppear = jQuery('.hiddenImage') ;

	jImageAppear.attr('src',name);	

	jImageDisappear.animate({
		opacity: 0
	}, 1000, function() {
		jImageDisappear.attr('class','hiddenImage');
	});

	jImageAppear.animate({
		opacity: 1
	}, 1000, function() {
		jImageAppear.attr('class','activeImage');
	});

}

function imageWaterTransition(name){
	var jImageAppear = jQuery('#water'),
	oppacityVal = name===undefined? 0:1;

	if (name!==undefined) jImageAppear.attr('src',name);

	jImageAppear.animate({opacity: oppacityVal}, 1000);
}

function imageSmokeTransition(name,oppacityVal){
	var jImageAppear = jQuery('#smoke'),
	oppacityVal = oppacityVal===undefined? 0:oppacityVal;

	if (name!==undefined) jImageAppear.attr('src',name);

	jImageAppear.animate({opacity: oppacityVal}, 1000);
}

function citySaturation(Svalue, Hvalue){
	var jImage = jQuery('.activeImage'),
	jWater = jQuery('#water'),
	jSmoke = jQuery('#smoke');

	jImage.css('filter','saturate('+Svalue+') sepia('+Hvalue+'%)');
	//jWater.css('filter','saturate('+Svalue+') sepia('+Hvalue+'%)');
	jSmoke.css('filter','saturate('+Svalue+') sepia('+Hvalue+'%)');
}

function scaleSun(value){
	var jSun = jQuery('#sun'),
	jValue = jQuery('#sun').css('background-image'),
	indexOfpx = jValue.indexOf('px'),
	current = parseInt(jValue.substring(16, indexOfpx)),
	difference = Math.abs(current-value),
	sign = current>value? -1 : +1 ,
	x=0;

	ScaleSunIteration(jSun,difference,sign,current,x);

}

function ScaleSunIteration(jSun,difference,sign,current,x){
	x = x+1
	if(x>difference) { return;}	
	jSun.css('background-image','unset');
	jSun.css('background-image','radial-gradient(circle ' + (current + x * sign) + 'px at 80% 20%, white, yellow, lightyellow, transparent)');
	setTimeout(function() {ScaleSunIteration(jSun,difference,sign,current,x)}, 50);
}

function small_map_economy(value) {return map_range(value, 0, 1, 0, maxEconomy);}
function small_map_pollution(value) {return map_range(value, 0, 1, 0, maxPollution);}

function map_range(value, low1, high1, low2, high2) {
	return low2 + (high2 - low2) * (value - low1) / (high1 - low1);
}		

function playMusic(){audio.play();}
function stopMusic(){audio.pause();}


function toggleAudioControl(){
	var image = $('#audioControll'),
	bool = $('#audioControll').attr('src') == 'img/volumeoff.svg';
	src = bool ? 'img/volumeon.svg':'img/volumeoff.svg';

	image.attr('src',src);

	if(bool) stopMusic();
	else playMusic();
}

// START!
getJson(init);
imagePreloader(0);
audio.volume = 0.2;
audio.play();
$('#audioControll').click(function(){console.log('ciao');toggleAudioControl();});